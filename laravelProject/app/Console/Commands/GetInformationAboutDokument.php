<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\XmlHelper;

class GetInformationAboutDokument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dokument:get-dokument';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get some information about dokument';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $filename = uniqid('', true) . '.txt';
        $file = fopen(public_path() . '/files/' . $filename, 'wb');

        $xmlDokuments = file_get_contents(public_path() . '/xml/' . 'Dokumenti.xml');
        $xmlPoslovniki = file_get_contents(public_path() . '/xml/' . 'Poslovniki.xml');
        $xmlPoglavja = file_get_contents(public_path() . '/xml/' . 'Poglavja.xml');
        $xmlSistemi = file_get_contents(public_path() . '/xml/' . 'Sistemi.xml');

        $parsingXmlDokumentsToArray = XmlHelper::xml2array($xmlDokuments);
        $parsingXmlPoslovnikiToArray = XmlHelper::xml2array($xmlPoslovniki);
        $parsingXmlPoglavjaToArray = XmlHelper::xml2array($xmlPoglavja);
        $parsingXmlSistemiToArray = XmlHelper::xml2array($xmlSistemi);

        $documentsArray = [];
        $documentsIdArray = [];
        $allDocumentsFilteredArray = [];

        $poslovnikiArray = [];
        $allPoslovnikiFilteredArray = [];

        $poglavjaArray = [];
        $allPoglavjaFilteredArray = [];

        $sistemiArray = [];
        $allSistemiFilteredArray = [];

        $combineArr = [];

        foreach($parsingXmlDokumentsToArray['FEED']['ENTRY'] as $items) {
            foreach($items as $key => $value) {
                if ($key === 'ID') {
                    $num = ltrim(strstr($value, '('), ')');
                    $id = mb_substr($num, 1, -1);
                    $documentsIdArray[] = $id;

                }

                if ($key === 'M:PROPERTIES') {
                    $documentsArray[$key][] = $value;
                } else if ($key === 'UPDATED') {
                    $documentsArray[$key][] = $value;
                } else if ($key === 'LINK') {
                    $documentsArray[$key][] = $value;
                }
            }

            for($i = 0; $i < count($documentsIdArray); $i++) {
                $allDocumentsFilteredArray['Document_id_'.$documentsIdArray[$i]] = [
                    'ID' => $documentsIdArray[$i],
                    'Updated' => $documentsArray['UPDATED'][$i],
                    'Naziv' => $documentsArray['M:PROPERTIES'][$i]['D:NAZIV'],
                    'Tip' => $documentsArray['M:PROPERTIES'][$i]['D:TIP'],
                    'Sistem' => $documentsArray['M:PROPERTIES'][$i]['D:SISTEM'],
                    'Stevilka' => $documentsArray['M:PROPERTIES'][$i]['D:STEVILKA'],
                    'Poslovnik_id' => $documentsArray['M:PROPERTIES'][$i]['D:POSLOVNIKID'],
                    'Sponzor' => [
                        $documentsArray['LINK'][$i]['2_attr']['REL'],
                        $documentsArray['LINK'][$i]['2_attr']['HREF']
                    ],
                    'Skrbnik' => [
                        $documentsArray['LINK'][$i]['3_attr']['REL'],
                        $documentsArray['LINK'][$i]['3_attr']['HREF']
                    ]
                ];
            }
        }

        foreach($parsingXmlPoslovnikiToArray['FEED']['ENTRY'] as $items) {
            foreach($items as $key => $value) {
                if ($key === 'CONTENT') {
                    $poslovnikiArray[$key][] = $value;
                } else if ($key === 'UPDATED') {
                    $poslovnikiArray[$key][] = $value;
                }
            }

            for($i = 0; $i < count($poslovnikiArray['UPDATED']); $i++) {
                $allPoslovnikiFilteredArray['Poslovnik_id_'.$poslovnikiArray['CONTENT'][$i]['M:PROPERTIES']['D:ID']] = [
                    'ID' => $poslovnikiArray['CONTENT'][$i]['M:PROPERTIES']['D:ID'],
                    'Naslov' => $poslovnikiArray['CONTENT'][$i]['M:PROPERTIES']['D:NASLOV'],
                    'Poglavje_id' => $poslovnikiArray['CONTENT'][$i]['M:PROPERTIES']['D:POGLAVJEID'],
                    'Updated' => $poslovnikiArray['UPDATED'][$i],
                ];
            }
        }

        foreach($parsingXmlPoglavjaToArray['FEED']['ENTRY'] as $items) {
            foreach($items as $key => $value) {
                if ($key === 'CONTENT') {
                    $poglavjaArray[$key][] = $value;
                } else if ($key === 'UPDATED') {
                    $poglavjaArray[$key][] = $value;
                }
            }

            for($i = 0; $i < count($poglavjaArray['UPDATED']); $i++) {
                $allPoglavjaFilteredArray['Poglavje_id_'.$poglavjaArray['CONTENT'][$i]['M:PROPERTIES']['D:ID']] = [
                    'ID' => $poglavjaArray['CONTENT'][$i]['M:PROPERTIES']['D:ID'],
                    'Naslov' => $poglavjaArray['CONTENT'][$i]['M:PROPERTIES']['D:NASLOV'],
                    'Sistem_id' => $poglavjaArray['CONTENT'][$i]['M:PROPERTIES']['D:SISTEMID'],
                    'Updated' => $poglavjaArray['UPDATED'][$i],
                ];
            }
        }

        foreach($parsingXmlSistemiToArray['FEED']['ENTRY'] as $items) {
            foreach($items as $key => $value) {
                if ($key === 'CONTENT') {
                    $sistemiArray[$key][] = $value;
                } else if ($key === 'UPDATED') {
                    $sistemiArray[$key][] = $value;
                }
            }

            for($i = 0; $i < count($sistemiArray['UPDATED']); $i++) {
                $allSistemiFilteredArray['Sistem_id_'.$sistemiArray['CONTENT'][$i]['M:PROPERTIES']['D:ID']] = [
                    'ID' => $sistemiArray['CONTENT'][$i]['M:PROPERTIES']['D:ID'],
                    'Naslov' => $sistemiArray['CONTENT'][$i]['M:PROPERTIES']['D:NASLOV'],
                    'Updated' => $sistemiArray['UPDATED'][$i],
                ];
            }
        }

        foreach($allSistemiFilteredArray as $sistemKey => $sistemValue) {
            $combineArr[$sistemKey] = $sistemValue;
            $combineArr[$sistemKey]['Poglavja'] = [];

            foreach($allPoglavjaFilteredArray as $poglavjeKey => $poglavjeValue) {

                if ($sistemValue['ID'] === $poglavjeValue['Sistem_id']) {
                    $combineArr[$sistemKey]['Poglavja'][$poglavjeKey] = $poglavjeValue;
                    $combineArr[$sistemKey]['Poglavja'][$poglavjeKey]['Poslovniki'] = [];

                    foreach ($allPoslovnikiFilteredArray as $poslovnikKey => $poslovnikValue) {

                        if ($poglavjeValue['ID'] === $poslovnikValue['Poglavje_id']) {
                            $combineArr[$sistemKey]['Poglavja'][$poglavjeKey]['Poslovniki'][$poslovnikKey] = $poslovnikValue;
                            $combineArr[$sistemKey]['Poglavja'][$poglavjeKey]['Poslovniki'][$poslovnikKey]['Documets'] = [];

                            foreach ($allDocumentsFilteredArray as $documentKey => $documentValue) {
                                if ($poslovnikValue['ID'] === $documentValue['Poslovnik_id']) {
                                    $combineArr[$sistemKey]['Poglavja'][$poglavjeKey]['Poslovniki'][$poslovnikKey]['Documets'][$documentKey] = $documentValue;
                                }
                            }
                        }

                    }
                }
            }
        }

        fwrite($file, var_export($combineArr, true));
        fclose($file);
    }
}
