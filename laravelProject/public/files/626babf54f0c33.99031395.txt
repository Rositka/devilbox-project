array (
  'Poslovnik_id_2' => 
  array (
    'ID' => '2',
    'Naslov' => 'Prevzem predmeta javnega naročila',
    'Poglavje_id' => '5',
    'Updated' => '2020-05-28T11:11:35+02:00',
  ),
  'Poslovnik_id_3' => 
  array (
    'ID' => '3',
    'Naslov' => 'Akt o sistemizaciji vrste del in delovnih mest družbe ELES, d.o.o., prečiščeno besedilo - 1',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:35+02:00',
  ),
  'Poslovnik_id_4' => 
  array (
    'ID' => '4',
    'Naslov' => 'Celovito načrtovanje in poročanje',
    'Poglavje_id' => '21',
    'Updated' => '2020-05-28T11:11:35+02:00',
  ),
  'Poslovnik_id_5' => 
  array (
    'ID' => '5',
    'Naslov' => 'Celovito obvladovanje investicijskih projektov',
    'Poglavje_id' => '17',
    'Updated' => '2020-05-28T11:11:35+02:00',
  ),
  'Poslovnik_id_6' => 
  array (
    'ID' => '6',
    'Naslov' => 'Diagnostika in izvajanje analiz stanja EEN',
    'Poglavje_id' => '17',
    'Updated' => '2020-05-28T11:11:36+02:00',
  ),
  'Poslovnik_id_7' => 
  array (
    'ID' => '7',
    'Naslov' => 'Določanje, dodeljevanje in obračun ČPZ',
    'Poglavje_id' => '1',
    'Updated' => '2020-05-28T11:11:36+02:00',
  ),
  'Poslovnik_id_8' => 
  array (
    'ID' => '8',
    'Naslov' => 'Etični kodeks družbe',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:36+02:00',
  ),
  'Poslovnik_id_10' => 
  array (
    'ID' => '10',
    'Naslov' => 'Izdelava modelov EES in analiz obratovanja EES',
    'Poglavje_id' => '2',
    'Updated' => '2020-05-28T11:11:36+02:00',
  ),
  'Poslovnik_id_11' => 
  array (
    'ID' => '11',
    'Naslov' => 'Izdelava obratovalnih navodil',
    'Poglavje_id' => '2',
    'Updated' => '2020-05-28T11:11:37+02:00',
  ),
  'Poslovnik_id_12' => 
  array (
    'ID' => '12',
    'Naslov' => 'Izdelava poročil o obratovanju EES',
    'Poglavje_id' => '2',
    'Updated' => '2020-05-28T11:11:37+02:00',
  ),
  'Poslovnik_id_13' => 
  array (
    'ID' => '13',
    'Naslov' => 'Izjava o varnosti z oceno tveganja IVOT - nepodpisan z aktivnimi povezavami',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:37+02:00',
  ),
  'Poslovnik_id_14' => 
  array (
    'ID' => '14',
    'Naslov' => 'Izvajanje pripravljenosti na domu',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:37+02:00',
  ),
  'Poslovnik_id_15' => 
  array (
    'ID' => '15',
    'Naslov' => 'Kolektivna pogodba elektrogospodarstva Slovenije',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:37+02:00',
  ),
  'Poslovnik_id_16' => 
  array (
    'ID' => '16',
    'Naslov' => 'Kolektivna pogodba ELES, UPB2',
    'Poglavje_id' => '19',
    'Updated' => '2020-08-19T16:05:41+02:00',
  ),
  'Poslovnik_id_17' => 
  array (
    'ID' => '17',
    'Naslov' => 'Komuniciranje',
    'Poglavje_id' => '20',
    'Updated' => '2020-05-28T11:11:38+02:00',
  ),
  'Poslovnik_id_18' => 
  array (
    'ID' => '18',
    'Naslov' => 'Kontrola vstopa in videonadzor',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:38+02:00',
  ),
  'Poslovnik_id_19' => 
  array (
    'ID' => '19',
    'Naslov' => 'Krovna varnostna politika ELES, d.o.o.',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:38+02:00',
  ),
  'Poslovnik_id_21' => 
  array (
    'ID' => '21',
    'Naslov' => 'Navodila naročnika za varovanje objektov Hajdrihova in Jamova',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:39+02:00',
  ),
  'Poslovnik_id_22' => 
  array (
    'ID' => '22',
    'Naslov' => 'Navodilo o objavljanju osmrtnic za zaposlene in upokojene delavce Elektro Slovenija d o o',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:39+02:00',
  ),
  'Poslovnik_id_23' => 
  array (
    'ID' => '23',
    'Naslov' => 'Notranja presoja',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:39+02:00',
  ),
  'Poslovnik_id_24' => 
  array (
    'ID' => '24',
    'Naslov' => 'Notranje revidiranje',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:39+02:00',
  ),
  'Poslovnik_id_25' => 
  array (
    'ID' => '25',
    'Naslov' => 'Obvladovanje finančno računovodskih procesov',
    'Poglavje_id' => '4',
    'Updated' => '2020-05-28T11:11:39+02:00',
  ),
  'Poslovnik_id_26' => 
  array (
    'ID' => '26',
    'Naslov' => 'Obvladovanje internih predpisov',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:40+02:00',
  ),
  'Poslovnik_id_27' => 
  array (
    'ID' => '27',
    'Naslov' => 'Obvladovanje izrednih razmer',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:40+02:00',
  ),
  'Poslovnik_id_28' => 
  array (
    'ID' => '28',
    'Naslov' => 'Obvladovanje javnih naročil',
    'Poglavje_id' => '5',
    'Updated' => '2020-05-28T11:11:40+02:00',
  ),
  'Poslovnik_id_29' => 
  array (
    'ID' => '29',
    'Naslov' => 'Obvladovanje kontrolne, merilne in preizkusne opreme',
    'Poglavje_id' => '9',
    'Updated' => '2020-05-28T11:11:40+02:00',
  ),
  'Poslovnik_id_30' => 
  array (
    'ID' => '30',
    'Naslov' => 'Obvladovanje pošte ter hramba in arhiviranje gradiva',
    'Poglavje_id' => '6',
    'Updated' => '2021-10-22T10:07:01+02:00',
  ),
  'Poslovnik_id_31' => 
  array (
    'ID' => '31',
    'Naslov' => 'Obvladovanje predpisov, standardov in drugih zahtev ter ocena ustreznosti',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:41+02:00',
  ),
  'Poslovnik_id_32' => 
  array (
    'ID' => '32',
    'Naslov' => 'Obvladovanje prodaj izven področja prometa z električno energjo',
    'Poglavje_id' => '5',
    'Updated' => '2020-05-28T11:11:41+02:00',
  ),
  'Poslovnik_id_33' => 
  array (
    'ID' => '33',
    'Naslov' => 'Obvladovanje števčnih podatkov',
    'Poglavje_id' => '2',
    'Updated' => '2020-05-28T11:11:41+02:00',
  ),
  'Poslovnik_id_34' => 
  array (
    'ID' => '34',
    'Naslov' => 'Obvladovanje tveganj',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:41+02:00',
  ),
  'Poslovnik_id_35' => 
  array (
    'ID' => '35',
    'Naslov' => 'Obvladovanje upravljanja s sredstvi',
    'Poglavje_id' => '17',
    'Updated' => '2020-05-28T11:11:41+02:00',
  ),
  'Poslovnik_id_36' => 
  array (
    'ID' => '36',
    'Naslov' => 'Obvladovanje varnosti in zdravja pri delu',
    'Poglavje_id' => '12',
    'Updated' => '2020-05-28T11:11:42+02:00',
  ),
  'Poslovnik_id_37' => 
  array (
    'ID' => '37',
    'Naslov' => 'Obvladovanje zavarovanj',
    'Poglavje_id' => '6',
    'Updated' => '2020-05-28T11:11:42+02:00',
  ),
  'Poslovnik_id_38' => 
  array (
    'ID' => '38',
    'Naslov' => 'Ocenjevanje stanja EES in delovanje  višjenivojskih aplikacij',
    'Poglavje_id' => '2',
    'Updated' => '2020-05-28T11:11:42+02:00',
  ),
  'Poslovnik_id_39' => 
  array (
    'ID' => '39',
    'Naslov' => 'Organizacija del v Centrih za infrastrukturo prenosnega omrežja',
    'Poglavje_id' => '18',
    'Updated' => '2020-05-28T11:11:42+02:00',
  ),
  'Poslovnik_id_40' => 
  array (
    'ID' => '40',
    'Naslov' => 'Overjanje števcev',
    'Poglavje_id' => '10',
    'Updated' => '2020-05-28T11:11:42+02:00',
  ),
  'Poslovnik_id_41' => 
  array (
    'ID' => '41',
    'Naslov' => 'Planiranje in operativno vodenje obratovanja prenosnega sistema',
    'Poglavje_id' => '2',
    'Updated' => '2020-05-28T11:11:43+02:00',
  ),
  'Poslovnik_id_42' => 
  array (
    'ID' => '42',
    'Naslov' => 'Politika upravljanja družbe ELES, d.o.o.',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:43+02:00',
  ),
  'Poslovnik_id_43' => 
  array (
    'ID' => '43',
    'Naslov' => 'Poslovnik kontrolnega laboratorija',
    'Poglavje_id' => '10',
    'Updated' => '2020-05-28T11:11:43+02:00',
  ),
  'Poslovnik_id_44' => 
  array (
    'ID' => '44',
    'Naslov' => 'Poslovnik o delu Projektnega sveta strateških projektov (PSSP)',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:43+02:00',
  ),
  'Poslovnik_id_45' => 
  array (
    'ID' => '45',
    'Naslov' => 'Poslovnik o delu Sveta in Tima za zagotavljanje informacijske varnosti (SIV in TIV)',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:43+02:00',
  ),
  'Poslovnik_id_46' => 
  array (
    'ID' => '46',
    'Naslov' => 'Poslovnik o delu Sveta strokovnih delavcev za varnost in zdravje pri delu (SVZD)',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:44+02:00',
  ),
  'Poslovnik_id_47' => 
  array (
    'ID' => '47',
    'Naslov' => 'Poslovnik o delu Sveta za koordinacijo mednarodnih aktivnosti',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:44+02:00',
  ),
  'Poslovnik_id_48' => 
  array (
    'ID' => '48',
    'Naslov' => 'Poslovnik o delu sveta za razvoj prenosnega omrežja, telekomunikacijske in informacijske tehnologije (SOTI)',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:44+02:00',
  ),
  'Poslovnik_id_49' => 
  array (
    'ID' => '49',
    'Naslov' => 'Poslovnik o načinu delovanja Sveta za sistem upravljanja',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:44+02:00',
  ),
  'Poslovnik_id_50' => 
  array (
    'ID' => '50',
    'Naslov' => 'Poslovnik o načinu vodenja družbe ELES, d.o.o.',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:44+02:00',
  ),
  'Poslovnik_id_51' => 
  array (
    'ID' => '51',
    'Naslov' => 'Poslovnik o načinu vodenja, koordiniranja in izvajanja mednarodnih aktivnosti družbe ELES, d.o.o.',
    'Poglavje_id' => '22',
    'Updated' => '2020-05-28T11:11:45+02:00',
  ),
  'Poslovnik_id_52' => 
  array (
    'ID' => '52',
    'Naslov' => 'Poslovnik sistema upravljanja',
    'Poglavje_id' => '15',
    'Updated' => '2020-05-28T11:11:45+02:00',
  ),
  'Poslovnik_id_53' => 
  array (
    'ID' => '53',
    'Naslov' => 'Požarni red družbe ELES, d.o.o',
    'Poglavje_id' => '12',
    'Updated' => '2020-09-08T13:35:38+02:00',
  ),
  'Poslovnik_id_54' => 
  array (
    'ID' => '54',
    'Naslov' => 'Pravilnik o Akademiji ELES',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:45+02:00',
  ),
  'Poslovnik_id_55' => 
  array (
    'ID' => '55',
    'Naslov' => 'Pravilnik o delovnem času',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:45+02:00',
  ),
  'Poslovnik_id_56' => 
  array (
    'ID' => '56',
    'Naslov' => 'Pravilnik o delu dijaka in študenta',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:46+02:00',
  ),
  'Poslovnik_id_57' => 
  array (
    'ID' => '57',
    'Naslov' => 'Pravilnik o dodeljevanju in uporabi poslovnih plačilnih kartic',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:46+02:00',
  ),
  'Poslovnik_id_58' => 
  array (
    'ID' => '58',
    'Naslov' => 'Pravilnik o doniranju in sponzoriranju',
    'Poglavje_id' => '20',
    'Updated' => '2020-05-28T11:11:46+02:00',
  ),
  'Poslovnik_id_59' => 
  array (
    'ID' => '59',
    'Naslov' => 'Pravilnik o finančnem in računovodskem poslovanju',
    'Poglavje_id' => '4',
    'Updated' => '2020-05-28T11:11:46+02:00',
  ),
  'Poslovnik_id_60' => 
  array (
    'ID' => '60',
    'Naslov' => 'Pravilnik o korporativni integriteti družbe ELES',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:46+02:00',
  ),
  'Poslovnik_id_61' => 
  array (
    'ID' => '61',
    'Naslov' => 'Pravilnik o obračunavanju varovanja pred tveganjem',
    'Poglavje_id' => '4',
    'Updated' => '2020-05-28T11:11:47+02:00',
  ),
  'Poslovnik_id_62' => 
  array (
    'ID' => '62',
    'Naslov' => 'Pravilnik o organizaciji ELES, d.o.o',
    'Poglavje_id' => '19',
    'Updated' => '2020-08-18T00:02:08+02:00',
  ),
  'Poslovnik_id_63' => 
  array (
    'ID' => '63',
    'Naslov' => 'Pravilnik o pooblastilih',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:47+02:00',
  ),
  'Poslovnik_id_64' => 
  array (
    'ID' => '64',
    'Naslov' => 'Pravilnik o popisu, odpisih in izločitvah sredstev in obveznosti',
    'Poglavje_id' => '4',
    'Updated' => '2020-05-28T11:11:47+02:00',
  ),
  'Poslovnik_id_65' => 
  array (
    'ID' => '65',
    'Naslov' => 'Pravilnik o postopku in načinu ugotavljanja ter dokazovanja alkoholiziranosti,',
    'Poglavje_id' => '12',
    'Updated' => '2020-05-28T11:11:47+02:00',
  ),
  'Poslovnik_id_66' => 
  array (
    'ID' => '66',
    'Naslov' => 'Pravilnik o priznanjih',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:48+02:00',
  ),
  'Poslovnik_id_67' => 
  array (
    'ID' => '67',
    'Naslov' => 'Pravilnik o službenih oblekah v družbi ELES, d.o.o',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:48+02:00',
  ),
  'Poslovnik_id_68' => 
  array (
    'ID' => '68',
    'Naslov' => 'Pravilnik o sodilih za razporejanje prihodkov, stroškov, odhodkov, sredstev in obveznosti',
    'Poglavje_id' => '4',
    'Updated' => '2020-05-28T11:11:48+02:00',
  ),
  'Poslovnik_id_69' => 
  array (
    'ID' => '69',
    'Naslov' => 'Pravilnik o stroških reprezentance',
    'Poglavje_id' => '20',
    'Updated' => '2020-05-28T11:11:48+02:00',
  ),
  'Poslovnik_id_70' => 
  array (
    'ID' => '70',
    'Naslov' => 'Pravilnik o uporabi počitniških enot družbe ELES, d.o.o.',
    'Poglavje_id' => '6',
    'Updated' => '2020-05-28T11:11:48+02:00',
  ),
  'Poslovnik_id_71' => 
  array (
    'ID' => '71',
    'Naslov' => 'Pravilnik o uporabi službenih vozil v službene in privatne namene',
    'Poglavje_id' => '6',
    'Updated' => '2020-05-28T11:11:49+02:00',
  ),
  'Poslovnik_id_72' => 
  array (
    'ID' => '72',
    'Naslov' => 'Pravilnik o varovanju osebnih podatkov',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:49+02:00',
  ),
  'Poslovnik_id_73' => 
  array (
    'ID' => '73',
    'Naslov' => 'Pravilnik o varstvu delavcev pred mobingom',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:49+02:00',
  ),
  'Poslovnik_id_74' => 
  array (
    'ID' => '74',
    'Naslov' => 'Prečiščeno besedilo Akta o ustanovitvi družbe ELES, d.o.o., sistemski operater prenosnega elektroenergetskega omrežja',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:49+02:00',
  ),
  'Poslovnik_id_75' => 
  array (
    'ID' => '75',
    'Naslov' => 'Ravnanje z informacijami javnega značaja',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:49+02:00',
  ),
  'Poslovnik_id_76' => 
  array (
    'ID' => '76',
    'Naslov' => 'Ravnanje z okoljem',
    'Poglavje_id' => '11',
    'Updated' => '2020-05-28T11:11:50+02:00',
  ),
  'Poslovnik_id_77' => 
  array (
    'ID' => '77',
    'Naslov' => 'Reševanje pritožb in prizivov kontrolnega laboratorija',
    'Poglavje_id' => '10',
    'Updated' => '2021-11-26T09:54:03+01:00',
  ),
  'Poslovnik_id_78' => 
  array (
    'ID' => '78',
    'Naslov' => 'Reševanje reklamacij odjemalcev in neskladnosti ter korektivni in preventivni ukrepi',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:50+02:00',
  ),
  'Poslovnik_id_79' => 
  array (
    'ID' => '79',
    'Naslov' => 'Skladiščno poslovanje',
    'Poglavje_id' => '5',
    'Updated' => '2020-05-28T11:11:50+02:00',
  ),
  'Poslovnik_id_80' => 
  array (
    'ID' => '80',
    'Naslov' => 'Skrbništvo osnovnih sredstev',
    'Poglavje_id' => '4',
    'Updated' => '2020-05-28T11:11:50+02:00',
  ),
  'Poslovnik_id_81' => 
  array (
    'ID' => '81',
    'Naslov' => 'Tehnična infrastruktura obratovanja',
    'Poglavje_id' => '9',
    'Updated' => '2020-05-28T11:11:50+02:00',
  ),
  'Poslovnik_id_82' => 
  array (
    'ID' => '82',
    'Naslov' => 'Temeljna listina NR',
    'Poglavje_id' => '19',
    'Updated' => '2020-05-28T11:11:51+02:00',
  ),
  'Poslovnik_id_83' => 
  array (
    'ID' => '83',
    'Naslov' => 'Uporaba celostne grafične podobe',
    'Poglavje_id' => '6',
    'Updated' => '2020-05-28T11:11:51+02:00',
  ),
  'Poslovnik_id_84' => 
  array (
    'ID' => '84',
    'Naslov' => 'Uporaba storitev mobilne telefonije',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:51+02:00',
  ),
  'Poslovnik_id_85' => 
  array (
    'ID' => '85',
    'Naslov' => 'Upravljanje in vzdrževanje stavb in z njimi povezanih objektov',
    'Poglavje_id' => '6',
    'Updated' => '2020-05-28T11:11:51+02:00',
  ),
  'Poslovnik_id_86' => 
  array (
    'ID' => '86',
    'Naslov' => 'Upravljanje inovacij',
    'Poglavje_id' => '24',
    'Updated' => '2020-05-28T11:11:51+02:00',
  ),
  'Poslovnik_id_87' => 
  array (
    'ID' => '87',
    'Naslov' => 'Upravljanje poslovnih procesov',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:52+02:00',
  ),
  'Poslovnik_id_88' => 
  array (
    'ID' => '88',
    'Naslov' => 'Upravljanje sistemskih storitev in izravnava odstopanj EES ',
    'Poglavje_id' => '3',
    'Updated' => '2021-05-24T15:03:14+02:00',
  ),
  'Poslovnik_id_89' => 
  array (
    'ID' => '89',
    'Naslov' => 'Upravljanje in nadzor elektroenergetske infrastrukture',
    'Poglavje_id' => '17',
    'Updated' => '2021-03-26T12:08:49+01:00',
  ),
  'Poslovnik_id_90' => 
  array (
    'ID' => '90',
    'Naslov' => 'Varovanje tajnih podatkov',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:52+02:00',
  ),
  'Poslovnik_id_91' => 
  array (
    'ID' => '91',
    'Naslov' => 'Varstvo poslovne skrivnosti',
    'Poglavje_id' => '13',
    'Updated' => '2020-05-28T11:11:52+02:00',
  ),
  'Poslovnik_id_92' => 
  array (
    'ID' => '92',
    'Naslov' => 'Vodenje graditve objekta',
    'Poglavje_id' => '16',
    'Updated' => '2020-05-28T11:11:52+02:00',
  ),
  'Poslovnik_id_93' => 
  array (
    'ID' => '93',
    'Naslov' => 'Vodenje relacij z regulatorjem in drugimi inštitucijami',
    'Poglavje_id' => '23',
    'Updated' => '2020-05-28T11:11:53+02:00',
  ),
  'Poslovnik_id_94' => 
  array (
    'ID' => '94',
    'Naslov' => 'Zagotavljanje opreme in storitev ITK',
    'Poglavje_id' => '8',
    'Updated' => '2020-05-28T11:11:53+02:00',
  ),
  'Poslovnik_id_95' => 
  array (
    'ID' => '95',
    'Naslov' => 'Zagotavljanje storitev avtoparka',
    'Poglavje_id' => '6',
    'Updated' => '2020-05-28T11:11:53+02:00',
  ),
  'Poslovnik_id_96' => 
  array (
    'ID' => '96',
    'Naslov' => 'Zaposlovanje, razvoj kadrov in obračun plač',
    'Poglavje_id' => '7',
    'Updated' => '2020-05-28T11:11:53+02:00',
  ),
  'Poslovnik_id_97' => 
  array (
    'ID' => '97',
    'Naslov' => 'Pravilnik o opravljanju dela na domu',
    'Poglavje_id' => '7',
    'Updated' => '2020-10-23T15:09:31+02:00',
  ),
  'Poslovnik_id_98' => 
  array (
    'ID' => '98',
    'Naslov' => 'Upravljanje informacijskih varnostnih incidentov',
    'Poglavje_id' => '13',
    'Updated' => '2021-01-25T08:27:53+01:00',
  ),
  'Poslovnik_id_99' => 
  array (
    'ID' => '99',
    'Naslov' => 'Upravljanje neprekinjenega poslovanja bistvenih storitev',
    'Poglavje_id' => '13',
    'Updated' => '2021-01-25T10:08:46+01:00',
  ),
  'Poslovnik_id_100' => 
  array (
    'ID' => '100',
    'Naslov' => 'Pravilnik o uporabi polnilnih postaj in kartic E8 RFID',
    'Poglavje_id' => '6',
    'Updated' => '2021-02-12T08:51:05+01:00',
  ),
  'Poslovnik_id_101' => 
  array (
    'ID' => '101',
    'Naslov' => 'Upravljaje članstva v mednarodnih delovnih skupinah',
    'Poglavje_id' => '7',
    'Updated' => '2021-04-19T09:20:01+02:00',
  ),
  'Poslovnik_id_102' => 
  array (
    'ID' => '102',
    'Naslov' => 'Upravljanje s štampiljkami',
    'Poglavje_id' => '6',
    'Updated' => '2021-06-22T10:19:42+02:00',
  ),
  'Poslovnik_id_103' => 
  array (
    'ID' => '103',
    'Naslov' => 'Poslovnik o delu sveta za raziskovalno in znanstveno dejavnost',
    'Poglavje_id' => '22',
    'Updated' => '2022-01-28T09:43:04+01:00',
  ),
)